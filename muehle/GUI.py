# Import and initialize the pygame library
import pygame
from MuehleGameBoard import DrawBoard
from Handler import Handler
from GameSetter import GameSetter

# Use this File to launch the Game 

running = True
while running:

    # Get gygame
    pygame.init()

    # Create Board
    dBoard = DrawBoard(pygame)

    # Set Game Mode
    gameSetter = GameSetter(pygame, dBoard)
    gameSetter.draw()
    gameSetter.startSetter()

    if gameSetter.running == False:
        running = False
        break

    gameSetter.unDraw()

    # Draw Board
    dBoard.draw()

    # Start Handler and Game
    handler = Handler(pygame,dBoard, gameSetter.gameMode)
    handler.start()

    if handler.running == False:
        running = False
        break

    gameSetter.unDraw()
    gameSetter.drawEnd(handler.winner)
    gameSetter.startEnd()

    if gameSetter.running == False:
        running = False
        

# ToDO : Gamemode Player Vs Ai and switch for this

# Done! Time to quit.
pygame.quit()
