
# This class uses the DrawBoard Class and the pygame Framework 
# To draw on the screen an Option to choose the game mode
# also it draws the end screen with the winner displayed
class GameSetter():

    # Consturctor, needs a pygame object and a DrawBoard object
    # Copys the draw board objects color vars to use as its own
    def __init__(self, pygame, DrawBoard):
        self.pygame = pygame
        self.dboard = DrawBoard
        self.set = False
        self.running = True
        self.gameMode = 0 # 0 vs Player 1 vs AI

        #GameMode Rectangles
        self.rect1 = self.pygame.Rect( self.dboard.SCREEN_WIDTH / 16 * 3, self.dboard.SCREEN_HEIGHT / 16 * 5,200 , 200)
        self.rect2 = self.pygame.Rect( self.dboard.SCREEN_WIDTH / 16 * 10, self.dboard.SCREEN_HEIGHT / 16 * 5,200 , 200)
        
        #Restart Tangle
        self.rect3 = 0

        self.colorText = self.dboard.colorText
        self.screenColor = self.dboard.screenColor
        self.ColorPlayerWhite = self.dboard.ColorPlayerWhite
        self.ColorPlayerBlack = self.dboard.ColorPlayerBlack
        self.ColorPlayerDefault  = self.dboard.ColorPlayerDefault
        self.ColorRect = self.dboard.ColorRect

    # draws to rectangles with the gamemode "player vs player" and "payer vs ai"
    # on the screen
    def draw(self):

        #Draw GameMode Rectangles
        self.pygame.draw.rect(self.dboard.screen, self.ColorRect, self.rect1, 10, border_radius=1)
        self.pygame.draw.rect(self.dboard.screen, self.ColorRect, self.rect2, 10, border_radius=1)

        # To Display Tesxt
        font = self.pygame.font.Font(None, 30) 
        text = font.render("Player vs Player", 1, self.colorText)

        # Determine the location that should be allocated for the text
        (x,y) = self.rect1.center
        text_box = text.get_rect(centerx=x, centery = y)

        # Draw the text onto the background
        self.dboard.screen.blit(text, text_box)

        # To Display Tesxt
        font = self.pygame.font.Font(None, 30) 
        text = font.render("Player vs AI", 1, self.colorText)

        # Determine the location that should be allocated for the text
        (x,y) = self.rect2.center
        text_box = text.get_rect(centerx=x, centery=y)

        # Draw the text onto the background
        self.dboard.screen.blit(text, text_box)
        
        #Updates the screen
        self.dboard.pygame.display.flip()   
    
    # fills the screen with black and removes all draws figures this way
    def unDraw(self):
        #remove drawn rectangle around circle
        self.dboard.screen.fill(self.screenColor)

        #Updates the screen
        self.dboard.pygame.display.flip()  

    # Waits for an input which is closing the winow or MouseButtonDown
    # starts checkPressedButton if MouseButtonDown happend
    def startSetter(self):

        while not self.set:

            for event in self.pygame.event.get():
                # Did the user click the window close button?
                if event.type == self.pygame.QUIT:
                    self.set = True
                    self.running = False
                if event.type == self.pygame.MOUSEBUTTONDOWN:
                    pos = self.pygame.mouse.get_pos()
                    self.checkPressedButton(pos)

    # checks if the pos of the click happend at one of the rectangles
    # stops startSetter if this is true and sets the gamemode
    def checkPressedButton(self,pos):
        (p1,p2) = pos
        (x1,y1) = self.rect1.center
        (x2,y2) = self.rect2.center

        if (x1 - 100) <= p1 and (x1 + 100) >= p1 and (y1 - 100) <= p2 and (y1 + 100) >= p2:
            self.gameMode = 0
            self.set = True
        if (x2 - 100) <= p1 and (x2 + 100) >= p1 and (y2 - 100) <= p2 and (y2 + 100) >= p2:
            self.gameMode = 1
            self.set = True

    # draws the end screen with the winner on it, winner can be -1,0,1
    def drawEnd(self, winner):
        heading = "It is a Draw"

        if winner == -1:
            heading = "Red wins"
        if winner == 1:
            heading = "Blue wins"

        #sets the color
        color = self.ColorPlayerDefault # default
        if winner == 1:
            color = self.ColorPlayerWhite # almost white
        elif winner == -1:
            color = self.ColorPlayerBlack # black

        rect = self.pygame.Rect(self.dboard.SCREEN_WIDTH / 16 * 5, self.dboard.SCREEN_HEIGHT / 16 * 5,300 , 200)

        #Draw GameMode Rectangles
        self.pygame.draw.rect(self.dboard.screen, color, rect , 10, border_radius=1)

        # To Display Tesxt
        font = self.pygame.font.Font(None, 30) 
        text = font.render(heading, 1, self.colorText)

        # Determine the location that should be allocated for the text
        (x,y) = rect.center
        text_box = text.get_rect(centerx= x, centery = y)

        # Draw the text onto the background
        self.dboard.screen.blit(text, text_box)

        self.rect3 = self.pygame.Rect( self.dboard.SCREEN_WIDTH / 64 * 27, self.dboard.SCREEN_HEIGHT / 16 * 10,100 , 50)

        #Draw Restart Rectangles
        self.pygame.draw.rect(self.dboard.screen, (0,255,0), self.rect3 , 10, border_radius=1)

        # To Display Tesxt
        font = self.pygame.font.Font(None, 30) 
        text = font.render("Restart", 1, self.colorText)

        # Determine the location that should be allocated for the text
        (x,y) = self.rect3.center
        text_box = text.get_rect(centerx= x, centery = y)

        # Draw the text onto the background
        self.dboard.screen.blit(text, text_box)

        #Updates the screen
        self.dboard.pygame.display.flip()

    # Waits for an input which is closing the winow or MouseButtonDown
    # if mouseButtonDown happend at the restart rectangle, return happens
    def startEnd(self):
         while self.running:

            for event in self.pygame.event.get():
                # Did the user click the window close button?
                if event.type == self.pygame.QUIT:
                    self.running = False
                if event.type == self.pygame.MOUSEBUTTONDOWN:
                    pos = self.pygame.mouse.get_pos()
                    (p1,p2) = pos
                    (x,y) = self.rect3.center
                    if (x-50) <= p1 and (x+50) >= p1 and (y-25) <= p2 and (y+25) >= p2:
                        return 
        