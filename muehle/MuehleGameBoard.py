
# Use this class to draw the game board of Muehle onto the screen
# Has Methods to show all stats of a Muehle game
class DrawBoard():

    # Constructor of the class. Initializes a pygame screen and also it sets all color vars and the vars for
    # all possible positions of game pieces 
    # The positions are 0 to 23 and defined as in the board class of MueheleLogic
    def __init__(self, pygame):

        self.pygame = pygame

        self.screenColor = (0,0,0)

        self.SCREEN_WIDTH = 900
        self.SCREEN_HEIGHT = 700

        self.rect1 = self.pygame.Rect(self.SCREEN_WIDTH/2-300,self.SCREEN_HEIGHT/2-275,600,600)
        self.rect2 = self.pygame.Rect(self.SCREEN_WIDTH / 2 - 175, self.SCREEN_HEIGHT / 2 - 137.5, 375, 375)
        self.rect3 = self.pygame.Rect(self.SCREEN_WIDTH / 2 - 87.5, self.SCREEN_HEIGHT / 2 - 68.75, 200, 200)
        self.rect2.center = self.rect1.center
        self.rect3.center = self.rect2.center

        self.screen = self.pygame.display.set_mode([self.SCREEN_WIDTH, self.SCREEN_HEIGHT])
        self.screen.fill(self.screenColor)

        # All places as in GameBoard
        self.place = [0]*24
        self.place[0] = self.rect1.topleft
        self.place[1] = self.rect1.midtop
        self.place[2] = self.rect1.topright
        self.place[3] = self.rect1.midright
        self.place[4] = self.rect1.bottomright
        self.place[5] = self.rect1.midbottom
        self.place[6] = self.rect1.bottomleft
        self.place[7] = self.rect1.midleft
        self.place[8] = self.rect2.topleft
        self.place[9] = self.rect2.midtop
        self.place[10] = self.rect2.topright
        self.place[11] = self.rect2.midright
        self.place[12] = self.rect2.bottomright
        self.place[13] = self.rect2.midbottom
        self.place[14] = self.rect2.bottomleft
        self.place[15] = self.rect2.midleft
        self.place[16] = self.rect3.topleft
        self.place[17] = self.rect3.midtop
        self.place[18] = self.rect3.topright
        self.place[19] = self.rect3.midright
        self.place[20] = self.rect3.bottomright
        self.place[21] = self.rect3.midbottom
        self.place[22] = self.rect3.bottomleft
        self.place[23] = self.rect3.midleft

        self.colorText = (255,255,255)
        self.ColorPlayerWhite = (0,0,255)
        self.ColorPlayerBlack = (255,0,0)
        self.ColorPlayerDefault  = (255,255,255)
        self.ColorRect = (255, 255, 255)
        self.CircleColorOthers = (0, 255, 0)
        self.CircleColorSelf =  (255,255,0)

        # The colors at these places
        self.placeColor = [self.ColorPlayerDefault]*24
        
        # The rectangles around there circles
        self.rectOfCircle = [0]*24

    # Draws the Gameboard onto the screen with all pieces at their positions
    def draw(self):
        
        # Draws the rectangles of the Board
        self.pygame.draw.rect(self.screen, self.ColorRect, self.rect1, 10, border_radius=1)
        self.pygame.draw.rect(self.screen, self.ColorRect, self.rect2, 10, border_radius=1)
        self.pygame.draw.rect(self.screen, self.ColorRect, self.rect3, 10, border_radius=1)

        # Draws the lines between the rectangles
        self.pygame.draw.line(self.screen,self.ColorRect,self.rect1.midtop, self.rect2.midtop,10)
        self.pygame.draw.line(self.screen,self.ColorRect, self.rect1.midbottom, self.rect2.midbottom,10)
        self.pygame.draw.line(self.screen,self.ColorRect, self.rect1.midleft, self.rect2.midleft, 10)
        self.pygame.draw.line(self.screen,self.ColorRect, self.rect1.midright, self.rect2.midright, 10)
        self.pygame.draw.line(self.screen,self.ColorRect, self.rect2.midtop, self.rect3.midtop, 10)
        self.pygame.draw.line(self.screen,self.ColorRect, self.rect2.midbottom, self.rect3.midbottom, 10)
        self.pygame.draw.line(self.screen,self.ColorRect, self.rect2.midleft, self.rect3.midleft, 10)
        self.pygame.draw.line(self.screen,self.ColorRect, self.rect2.midright, self.rect3.midright, 10)

        #Draws the circles on the outher rectangle rect1 
        for i in range(0,8):
            self.rectOfCircle[i] = self.pygame.draw.circle(self.screen, self.placeColor[i], self.place[i], 15)

        #Draws the circles on the middle rectangle rect2 
        for i in range(8,16):
            self.rectOfCircle[i] = self.pygame.draw.circle(self.screen, self.placeColor[i], self.place[i], 15)

        #Draws the circles on the inner rectangle rect3
        for i in range(16,24):
            self.rectOfCircle[i] = self.pygame.draw.circle(self.screen, self.placeColor[i], self.place[i], 15)

        #Updates the screen
        self.pygame.display.flip()   

    # Changes a color of one piece of player at place
    def changeColor(self, player, place):
        #place 0 -23

        #sets the color
        color = self.ColorPlayerBlack # black
        if player == 1:
            color = self.ColorPlayerWhite # almost white
        elif player == 0:
            color = self.ColorPlayerDefault # default color
        
        # changes the circles color
        self.pygame.draw.circle(self.screen, color, self.place[place], 15)
        
        # saves the changed Color
        self.placeColor[place] = color

        # updates the screen
        self.pygame.display.flip() 

    # Marks a piece on the board at place in yellow
    def markCircle(self, place):
        #draw rectangle around circle
        self.pygame.draw.rect(self.screen, self.CircleColorSelf, self.rectOfCircle[place], 4, border_radius=1)

        #Updates the screen
        self.pygame.display.flip()

    # Marks all pieces of the same color as player in green
    def markOtherPlayerCircles(self,player):

        color = (0,0,0) # black
        if player == 1:
            color = (0,255,255) # almost white
        elif player == 0:
            color = (0,0,255) # default color

        for i in range(len(self.place)):
            if  color == self.placeColor[i]:
                #draw rectangle around circle
                self.pygame.draw.rect(self.screen, self.CircleColorOthers, self.rectOfCircle[i], 4, border_radius=1)
        
        #Updates the screen
        self.pygame.display.flip()

    # Marks all pieces on the places list in green
    def markRangeCircles(self, places):

        for place in places:
            #draw rectangle around circle
            self.pygame.draw.rect(self.screen, self.CircleColorOthers, self.rectOfCircle[place], 4, border_radius=1)
        
        #Updates the screen
        self.pygame.display.flip()

    # Remove the marks of all marked pieces
    def unMarkCircle(self):
        #remove drawn rectangle around circle
        self.screen.fill(self.screenColor)

        # reDraw everything
        self.draw()

    # Sets the text which states which players turn it is and how many pieces they have left
    def setPlayerText(self, player, piecesLeft):
        # Redrawing everything
        self.screen.fill(self.screenColor)
        self.draw()
        
        heading = "Player 1 (Red):"
        if player == 1:
            heading = "Player 2 (Blue):"


        # To Display Text Player
        font = self.pygame.font.Font(None, 30) 
        text = font.render(heading, 1, self.colorText)

        # Determine the location that should be allocated for the text
        text_box = text.get_rect(centerx=100)

        

        # Draw the text onto the background
        self.screen.blit(text, text_box)

        # To Display Text Pieces left
        if piecesLeft != 0:
            font = self.pygame.font.Font(None, 30) 
            text = font.render("Pieces left: {piecesLeft}".format(piecesLeft = piecesLeft ), 1, self.colorText)

            # Determine the location that should be allocated for the text
            text_box = text.get_rect(centerx=400)

            # Draw the text onto the background
            self.screen.blit(text, text_box)

        self.pygame.display.flip() 

        
 
