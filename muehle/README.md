# Muehle implementation for Alpha Zero General

An implementation of the Board game Muehle for an university project. It's based of the implementations of TicTacToe and Othello, which you can find in the Root folder

To train a model for Muehle, change the imports in ```main.py``` to:
```python
from Coach import Coach
from muehle.MuehleGame import MuehleGame as Game
from muehle.pytorch.NNet import NNetWrapper as nn
from utils import *
```

and the first line of ```__main__``` to
```python
g = Game()
```
```pit.py``` hasn't been implemented for this game

To start training a model for Muehle:
```bash
python main.py
```



### Experiments
We trained a pytorch model for Muehle (45 iterations, 10 episodes, 10 epochs per iteration and 50 MCTS simulations per turn). This took about 28 hours on GTX 1070 with CUDA. The pretrained model (pytorch) can be found in ```alpha-zero-general/best.pth.tar```.
You can play against running the command
```bash
python GUI.py
```
in the  ```alpha-zero-general/muehle``` directory.


### Contributors and Credits
* [Fabian Speh](https://git.thm.de/fsph90)
* [Oliver Kamrath](https://git.thm.de/okmr85)

The implementation is based on the game of Othello and TicTacToe (https://github.com/suragnair/alpha-zero-general/tree/master/othello).

### AlphaGo / AlphaZero Talks
* February 8, 2018 - [Advanced Spark/Tensorflow Meetup at Thumbtack](https://www.meetup.com/Advanced-Spark-and-TensorFlow-Meetup/events/245308722/): [Youtube](https://youtu.be/dhmBrTouCKk?t=1017) / [Slides](http://static.brettkoonce.com/presentations/go_v1.pdf)
* March 6, 2018 - [Advanced Spark/Tensorflow Meetup at Strata San Jose](https://www.meetup.com/Advanced-Spark-and-TensorFlow-Meetup/events/246530339/): [Youtube](https://www.youtube.com/watch?time_continue=1257&v=hw9VccUyXdY) / [Slides](http://static.brettkoonce.com/presentations/go.pdf)
