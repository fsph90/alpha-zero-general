'''
Board class for the game of TicTacToe.
Default board size is 3x3.
Board data:
  1=white(O), -1=black(X), 0=empty
  first dim is column , 2nd is row:
     pieces[0][0] is the top left square,
     pieces[2][0] is the bottom left square,
Squares are stored and manipulated as (x,y) tuples.

Author: Evgeny Tyurin, github.com/evg-tyurin
Date: Jan 5, 2018.

Based on the board for the game of Othello by Eric P. Nichols.

'''
# from bkcharts.attributes import color

# static number of Fields available in Mühle
NUMBER_OF_FIELDS = 24

class Board:

    def __init__(self):
        "Set up initial board configuration."

        self.remove_piece = False
        # Creates an empty list where every field represents one field on the board
        self.pieces = [0]*NUMBER_OF_FIELDS

        # dictionary which represents in which phase the players are in
        self.phase = {-1: 1, 1: 1}

        self.moves_without_mills = 0

        # dictionary which represents the numbers of pieces left in the 1st phase
        self.pieces_left = {-1: 9, 1: 9}

        # dictionary with the neighbors for every field
        self.neighbors = {
                0:[1, 7],
                1:[0, 2, 9],
                2:[1, 3],
                3:[2, 4, 11],
                4:[3, 5],
                5:[4, 6, 13],
                6:[5, 7],
                7:[0, 6, 15],
                8:[9, 15],
                9:[1, 8, 10, 17],
                10:[9, 11],
                11:[3, 10, 12, 19],
                12:[11, 13],
                13:[5, 12, 14, 21],
                14:[13, 15],
                15:[7, 8, 14, 23],
                16:[17, 23],
                17:[9, 16, 18],
                18:[17, 19],
                19:[11, 18, 20],
                20:[19, 21],
                21:[13, 20, 22],
                22:[21, 23],
                23:[15, 16, 22] }

        # list, which contains every possible mill

        self.mills =[(0, 1, 2), (2, 3, 4), (4, 5, 6),
                     (0, 6, 7), (1, 9, 17), (3, 11, 19),
                     (5, 13, 21),(7, 15, 23),(8, 9, 10),
                     (10, 11, 12), (12, 13, 14), (14, 15, 8),
                     (16, 23, 22), (16, 17, 18), (18, 19, 20), (20, 21, 22)]


    # add [][] indexer syntax to the Board
    def __getitem__(self, index):
        return self.pieces[index]

    def get_legal_moves(self, color):
        """Returns all the legal moves for the given color.
            (1 for white, -1 for black)
        """
        if self.remove_piece:
            enemy_mills = {}
            moves = set()
            for x in range(NUMBER_OF_FIELDS):
                if self.pieces[x] == -color:
                    enemy_mills[x] = self.is_mill(x, -color)

            if all(enemy_mills.values()):

                value = [(x, -1) for x in enemy_mills.keys()]

                moves = set(value)

            else:
                value = [(x, -1) for x in enemy_mills.keys() if not enemy_mills[x]]

                moves = set(value)
        else:
            if self.phase[color] == 1:
                moves = set()  # stores the legal moves.

                # Get all the empty squares (color==0)
                for x in range(NUMBER_OF_FIELDS):

                    if self.pieces[x] == 0:
                        newmove = (-1, x)
                        moves.add(newmove)

            if self.phase[color] == 2:
                moves = set()

                for x in range(NUMBER_OF_FIELDS):
                    if self.pieces[x] == color:
                        for y in self.neighbors[x]:
                            if self.pieces[y] == 0:
                                 newmove = (x, y)
                                 moves.add(newmove)

            if self.phase[color] == 3:

                own_pieces =[]
                free_fields =[]
                for x in range(NUMBER_OF_FIELDS):
                    if self.pieces[x] == color:
                        own_pieces.append(x)
                    elif self.pieces[x] == 0:
                        free_fields.append(x)

                moves =[(x, y) for x in own_pieces for y in free_fields]
                moves = set(moves)

        return list(moves)

    # checks if current player has any legal moves
    def has_legal_moves(self, color):

        if len(self.get_legal_moves(color)) > 0:

            return True
        return False

    # counts number of  pieces for specified player
    # only relevant phase == 2 phase == 3
    def count_pieces(self, color):

        piece_count = 0
        for x in range(NUMBER_OF_FIELDS):
            if self.pieces[x] == color:
                piece_count += 1

        return piece_count

    '''runs after every move,
       updates the phase of a player if condition is met '''
    def update_phase(self):

        if self.phase[-1] == 1 and self.pieces_left[-1] == 0:
            self.phase[-1] = 2

        if self.phase[1] == 1 and self.pieces_left[1] == 0:
            self.phase[1] = 2

        if self.phase[-1] == 2 and self.count_pieces(-1) <= 3:
            self.phase[-1] = 3

        if self.phase[1] == 2 and self.count_pieces(1) <= 3:
            self.phase[1] = 3

    # uses list mills to check if move leads to mill, returns True if yes
    def is_mill(self, field, color):
        for mill in self.mills:
            if field in mill:
                (a, b, c) = mill

                if self.pieces[a] == color and self.pieces[b] == color and self.pieces[c] == color:
                    return True
        return False

    # checks if there hasnt been any mills in the last 50 moves and ends the game in a draw if True
    def is_draw(self):

        if self.moves_without_mills >= 50:
            return True

        return False

    # checks whether the opposing player has no more legal moves or less than 3 pieces
    def is_win(self, color):


        if self.phase[-color] > 1:
            if not self.has_legal_moves(-color) or self.count_pieces(-color) < 3:
                return True

        return False

    # executes a given move, functionality depends on the given move
    def execute_move(self, move, color,verbose = False):
        """Perform the given move on the board;
            y >= 0: all the moves, where a piece is added to the board or moved to another field
            else:   all the moves, where a piece is removed from the board
        color gives the color pf the piece to play (1=white,-1=black)
        """
        (x, y) = move

        if y >= 0:

            if x == -1:
                self.pieces[y] = color
                self.pieces_left[color] -= 1

                self.moves_without_mills += 1
            else:
                self.pieces[x] = 0
                self.pieces[y] = color

                self.moves_without_mills += 1

            if self.is_mill(y, color):
                self.remove_piece = True
                self.moves_without_mills = 0

        else:
            self.pieces[x] = 0
            self.remove_piece = False