import sys
from MuehleLogic import Board
from MuehleGame import MuehleGame

from pytorch.NNet import NNetWrapper as NNet

from utils import *
from MCTS import MCTS
import numpy as np

# A class used to handle all events while playing the game Muhle
class Handler():

    # Constoructor of the class, gets an pygame object and a DrawBoard object to do the requiered functions. It also gets the Game mode which should be set
    # 1 for Player vs AI, 0 for Player Vs Player
    # Creats a new Board object of the class Board which is used to make the calcualtions of possible moves and to represent the logic of the game
    def __init__(self, pygame, DrawBoard, gameMode):
        self.dBoard = DrawBoard
        self.pygame = pygame
        self.gameMode = gameMode
        
        self.player = 1
        self.inPhase = False
        self.createdMill = False

        self.winner = 0

        self.board = Board()
        self.muehleGame = MuehleGame()

        if self.gameMode == 1:
            n1 = NNet(self.muehleGame)
            n1.load_checkpoint('','best.pth.tar')
            args1 = dotdict({'numMCTSSims': 50, 'cpuct': 1.0})
            mcts1 = MCTS(self.muehleGame, n1, args1)
            self.ai_player = lambda x: np.argmax(mcts1.getActionProb(x, temp=0))


        self.playing = True
        self.running = True

        self.dBoard.setPlayerText(self.player, self.board.pieces_left[self.player])

    # Waits for an input of the player which can be either closing the game or pressing the mouse Button
    # in this case it starts the method checkPressedButton
    # Also all possible moves for the current player are marked
    def start(self):
        while self.running and self.playing:
            # mark legal moves

            self.markLegalMoves()

            for event in self.pygame.event.get():
                # Did the user click the window close button?
                if event.type == self.pygame.QUIT:
                    self.running = False
                if event.type == self.pygame.MOUSEBUTTONDOWN:
                    pos = self.pygame.mouse.get_pos()
                    self.checkPressedButton(pos)

    # Checks if the pos of the click happend at the possible location of a piece
    # If so it calls handleMove with the index of the pressed circle/piece
    def checkPressedButton(self,pos):
        (p1,p2) = pos

        for index in range(len(self.dBoard.place)):
            (x,y) = self.dBoard.place[index]
            if (x-9) <= p1 and (x+9) >= p1 and (y-9) <= p2 and (y+9) >= p2:
                self.handleMove(index)

    # checks if the pressed piece is part of a possible move 
    # it also checks the phase of the player for this
    # if it is valid the phaseXMove method is called 
    # if a move happend and the Gamemode is 1 the AiStuff method is called
    def handleMove(self, index):

        moves = self.board.get_legal_moves(self.player)
        valid = False
        phase = self.board.phase[self.player]

        if phase == 1: 
            for move in moves:
                (x,y) = move
                if y == index and x == -1:
                    valid = True
                    break
        elif phase == 2 or phase == 3:
            for move in moves:
                (x,y) = move
                if x == index:
                    valid = True
                    break
        

        if valid == False:
            return
        
        #check if move valid
        #check phase

        didMove = False

        # 1 -> execute move when vaild
        if phase == 1:
            didMove = self.phase1Move(index)

        elif phase == 2:
            didMove = self.phase2Move(index)

        elif phase == 3:
            didMove = self.phase3Move(index)
                
        # 2 / 3 -> mark, show possible places, wait for new input
        # -> execute move when vaild afert second input 

        if didMove is not True:
            return

        if self.gameMode == 1:
            self.AIstuff()
        # check Ai mode
        # Do AI stuff

    # Does a phase 1 move which is putting a new piece onto the board 
    # which means the color is changed and the move is executed bei Board.execute()
    # calls do Mill if one happend     
    # changes which players turn is atm 
    # returns true if nothing went wrong       
    def phase1Move(self, index):
        self.dBoard.changeColor(self.player, index)
        self.board.execute_move((-1,index), self.player)

        if self.board.remove_piece == True:
            self.doMill()

        # check if decided
        self.end()

        self.board.update_phase()
        self.player = -1 * self.player
        self.dBoard.setPlayerText(self.player, self.board.pieces_left[self.player])
        return True

    # Does a phase 2 move which is moving a piece
    # calls for this startSecondMove() to get second position of the circle/piece
    # marks all possible places for this first
    # if the move isnt abandond it executes and changes the colors of the fields 
    # changes whos players turn it is 
    def phase2Move(self, index):
        self.dBoard.unMarkCircle()
        self.dBoard.setPlayerText(self.player, 0)
        self.dBoard.markCircle(index)
        # get legal moves
        moves = self.board.get_legal_moves(self.player)
        secLegalMoves = []

        for move in moves:
            (x,y) = move
            if x == index:
                secLegalMoves.append(y)
        # mark legal moves
        self.dBoard.markRangeCircles(secLegalMoves)
        # wait input
        secMove = self.startSecondMove(index, secLegalMoves)

        if secMove == index:
            self.dBoard.unMarkCircle()
            self.dBoard.setPlayerText(self.player, 0)
            return False
        
        # ...

        # execute move
        self.board.execute_move((index, secMove), self.player)
        # reset Board
        self.dBoard.unMarkCircle()
        self.dBoard.changeColor(0, index)
        self.dBoard.changeColor(self.player, secMove)

        # do mill
        if self.board.remove_piece == True:
            self.doMill()
        
        # check if decided
        self.end()

        # change phase
        self.board.update_phase()
        # change player
        self.player = -1*self.player
        # changoe player text
        self.dBoard.setPlayerText(self.player, 0)
        return True

    # Does a phase 3 move which is moving a piece
    # calls for this startSecondMove() to get second position of the circle/piece
    # marks all possible places for this first
    # if the move isnt abandond it executes and changes the colors of the fields 
    # changes whos players turn it is 
    def phase3Move(self, index):
        self.dBoard.unMarkCircle()
        self.dBoard.setPlayerText(self.player, 0)
        self.dBoard.markCircle(index)
        
        secLegalMoves = []

        # get legal moves
        moves = self.board.get_legal_moves(self.player)
        for move in moves:
            (x,y) = move
            if x == index:
                secLegalMoves.append(y)

        # mark legal moves
        self.dBoard.markRangeCircles(secLegalMoves)

        # wait input
        secMove = self.startSecondMove(index, secLegalMoves)

        if secMove == index:
            self.dBoard.unMarkCircle()
            self.dBoard.setPlayerText(self.player, 0)
            return False
        
        # execute move
        self.board.execute_move((index, secMove), self.player)

        # reset Board
        self.dBoard.unMarkCircle()
        self.dBoard.changeColor(0, index)
        self.dBoard.changeColor(self.player, secMove)

        # do mill

        if self.board.remove_piece == True:
            self.doMill()
        
        # check if decided
        self.end()

        # change phase
        self.board.update_phase()
        # change player
        self.player = -1*self.player
        # changoe player text
        self.dBoard.setPlayerText(self.player, 0)
        return True

    # Waits for an input of either closing the programm of a pressed mouse button
    # if the click happend at the piece at index it returnx index
    # which means the move is abandond 
    # if it happends at a index of legalMoves it returns this index 
    # which means a place was choosen
    def startSecondMove(self, index, legalMoves):
        while self.running:

            for event in self.pygame.event.get():
                # Did the user click the window close button?
                if event.type == self.pygame.QUIT:
                    self.running = False
                
                if event.type == self.pygame.MOUSEBUTTONDOWN:
                    pos = self.pygame.mouse.get_pos()
                    (p1,p2) = pos

                    # Check if unmark
                    if index is not -1:
                        (x,y) = self.dBoard.place[index]
                        if (x-9) <= p1 and (x+9) >= p1 and (y-9) <= p2 and (y+9) >= p2:
                            return index

                    # Check if second Field
                    for i in legalMoves:
                        (x,y) = self.dBoard.place[i]
                        if (x-9) <= p1 and (x+9) >= p1 and (y-9) <= p2 and (y+9) >= p2:
                            return i

    # called when a mill happend
    # marks all pieces which can be removed 
    # calls startSecond move with index = -1 and all removeable pieces as legalMoves
    # the removes the returned choosen piece by changing color and calling executet
    def doMill(self):
        moves = self.board.get_legal_moves(self.player)
        self.dBoard.unMarkCircle()
        self.dBoard.setPlayerText(self.player, self.board.pieces_left[self.player])

        legalMoves = []
        for move in moves:
            (x,y) = move
            legalMoves.append(x)

        self.dBoard.markRangeCircles(legalMoves)
        index = self.startSecondMove(-1, legalMoves)

        # execute move
        self.board.execute_move((index, -1), self.player)

        # change color
        self.dBoard.changeColor(0, index)
    
    # Marks all possible moves of player, einer putting pieces at this place down or taking them to move
    def markLegalMoves(self):
        moves = self.board.get_legal_moves(self.player)
        legalMoves = []

        for move in moves:
            (x,y) = move
            if self.board.phase[self.player] == 1:
                legalMoves.append(y)
            else:
                legalMoves.append(x)
        # mark legal moves
        self.dBoard.markRangeCircles(legalMoves)

    # checks if the game ended and player won or a draw happend
    def end(self):
        if self.board.is_win(self.player):
            self.winner = self.player
            self.playing = False
            

        if self.board.is_draw():
            self.winner = 0
            self.playing = False

    # called to let the ai move
    # checks phase and the with getAIAction it gets the move of the AI 
    # executes the move
    # if a mill happend getAiActions is called again 
    # changes whos turn it is afterwards         
    def AIstuff(self):
        
        # check phase
        phase = self.board.phase[self.player]

        if phase == 1:
            move = self.getAiAction()
            (x,y) = move
            # ecexute move
            self.board.execute_move(move, self.player)
            self.dBoard.changeColor(self.player, y)
        
        else:
            move = self.getAiAction()
            (x,y) = move
            # ecexute move
            self.board.execute_move(move, self.player)
            self.dBoard.changeColor(self.player, y)
            self.dBoard.changeColor(0, x)

        # check mill -> execute mill
        if self.board.remove_piece == True:
            move = self.getAiAction()
            (x,y) = move
            # execute move
            self.board.execute_move(move, self.player)
            self.dBoard.changeColor(0, x)

        self.end()
        
        self.board.update_phase()
        self.player = -1 * self.player
        self.dBoard.setPlayerText(self.player, self.board.pieces_left[self.player])

    # gets the Action of the Ai Alpha Zero 
    def getAiAction(self):

        actions = self.muehleGame.getActionList()
        return actions[self.ai_player(self.muehleGame.getCanonicalForm(self.board, self.player))]
