from __future__ import print_function
import sys
sys.path.append('..')
from Game import Game
from .MuehleLogic import Board
import numpy as np
from copy import deepcopy

"""
Game class implementation for the game of TicTacToe.
Based on the OthelloGame then getGameEnded() was adapted to new rules.

Author: Evgeny Tyurin, github.com/evg-tyurin
Date: Jan 5, 2018.

Based on the OthelloGame by Surag Nair.
"""
NUMBER_OF_FIELDS = 24


class MuehleGame(Game):

    # stores all possible actions available in the game
    actionList = []

    # used to flip the board for the getSymmetries() function
    flipDict = {
        -1:-1,
        0:2,
        2:0,
        7:3,
        3:7,
        6:4,
        4:6,
        5:5,
        1:1,
        8:10,
        10:8,
        15:11,
        11:15,
        14:12,
        12:14,
        16:18,
        18:16,
        23:19,
        19:23,
        22:20,
        20:22,
        17:17,
        21:21,
        9:9,
        13:13

    }

    def __init__(self):

        self.actionList = self.getActionList()
        self.actionArray = self.getActionArray()

    # instantiates a Board() object and returns it in its inital from
    def getInitBoard(self):
        # return initial board
        b = Board()
        return b

    # stores all possible actions in a list and returns it
    def getActionList(self):

        fields = range(NUMBER_OF_FIELDS)

        phase1_moves = [(-1, y) for y in fields]

        phase3_moves = [(x, y) for x in fields for y in fields if x!=y]

        remove_moves =[(x, -1) for x in fields]

        all_moves = phase1_moves + phase3_moves + remove_moves

        return all_moves

#  helper for getValidMoves, so it can search faster for specific values
    def getActionArray(self):

        arr = np.ndarray(shape=(25,25), dtype=int)
        for i in range(len(self.actionList)):
            move = self.actionList[i]
            (x,y) = move
            arr[x][y] = i
        return arr

#  returns 24 as the number of fields on the board
    def getBoardSize(self):
        # all possible fields
        return 24

    # returns the length of the actionList
    def getActionSize(self):
        # 600
        return len(self.actionList)

    '''returns the board after a move has been taken. 
    player stays the same if lhe last move has resulted in a mill
    or changes the player if thats not the case'''
    def getNextState(self, board, player, action):

        # if player takes action on board, return next (board,player)
        # action must be a valid move
        move = self.actionList[action]

        new_board = deepcopy(board)

        new_board.execute_move(move, player)
        new_board.update_phase()

        if new_board.remove_piece:
            return new_board, player
        else:
            return new_board, -player

    def getValidMoves(self, board, player):

        valids = [0]*self.getActionSize()

        legalMoves = board.get_legal_moves(player)
        for x, y in legalMoves:
            # valid_move = self.actionList.index((x,y))
            valid_move = self.actionArray[x][y]

            valids[valid_move] = 1

        return np.array(valids)

    '''checks for the current state of the game.
       returns -1 or 1 if either player has won the game
       returns 0.0001 if the result is a draw
       returns 0 if the game hasnt finished yet '''
    def getGameEnded(self, board, player):
        # return 0 if not ended, 1 if player 1 won, -1 if player 1 lost
        # player = 1
        if board.is_win(player):
            return 1
        if board.is_win(-player):
            return -1
        if board.is_draw():
            return 1e-4
        if board.has_legal_moves(player):
            return 0

    # flips the values of the board to achieve a standardized form(player 1 looks like player -1)
    def getCanonicalForm(self, board, player):

        new_board = deepcopy(board)
        pieces_array = np.asarray(board.pieces)
        pieces_array = player*pieces_array
        new_board.pieces = list(pieces_array)

        if player == -1:
            new_board.phase[-1] = board.phase[1]
            new_board.phase[1] = board.phase[-1]

            new_board.pieces_left[-1] = board.pieces_left[1]
            new_board.pieces_left[1] = board.pieces_left[-1]

        return new_board

    # changes the position of the field indices to match a 90 degrees rotation
    def rotate_field_90(self, x):

        if 0 <= x <= 7:
            return (x+2) % 8

        elif 8 <= x <= 15:
            return 8+((x - 8 + 2) % 8)

        elif 16 <= x <= 23:
            return 16 + ((x - 16 + 2) % 8)

        else:
            return x

    # rotates the board and transfers the values to their new positions
    def rotate_board_90(self, board):
        newBoard = deepcopy(board)
        for x in range(len(board.pieces)):
            newBoard.pieces[self.rotate_field_90(x)] = board.pieces[x]

        return newBoard

    # flips the board on the horizontal axis using the flipDict declared as an attribute
    def flip_board_horizontally(self,board):
        newBoard = deepcopy(board)
        for x in range (len(board.pieces)):
            newBoard.pieces[self.flipDict[x]] = board.pieces[x]

        return newBoard

    # changes the values of the policy vector to match the rotated board
    def rotate_pi_90(self, pi):
        new_pi = pi.copy()
        for i in range(len(pi)):
            old_move = self.actionList[i]
            (old_x, old_y) = old_move
            new_x = self.rotate_field_90(old_x)
            new_y = self.rotate_field_90(old_y)
            new_index = self.actionArray[new_x][new_y]
            new_pi[new_index] = pi[i]

        return new_pi

    # changes the values of the policy vector to match the flipped board
    def flip_pi_horizontally(self, pi):
        new_pi = pi.copy()
        for i in range(len(pi)):
            old_move = self.actionList[i]
            (old_x, old_y) = old_move
            new_x = self.flipDict[old_x]
            new_y = self.flipDict[old_y]
            new_index = self.actionArray[new_x][new_y]
            new_pi[new_index] = pi[i]

        return new_pi

    '''creates variations of a board state through rotating and flipping it (they are technically the same)
       all variations are used as training data, which leads to much bigger efficiency'''
    def getSymmetries(self, board, pi):

        l =[(board,pi)]

        l.append((self.flip_board_horizontally(board),self.flip_pi_horizontally(pi)))
        prev_board = board
        prev_pi = pi
        for i in range(1,4):
            prev_board = self.rotate_board_90(prev_board)
            prev_pi = self.rotate_pi_90(prev_pi)
            l.append((prev_board, prev_pi))
            l.append((self.flip_board_horizontally(prev_board),self.flip_pi_horizontally(prev_pi)))


        return l

    def stringRepresentation(self, board):
        # 8x8 numpy array (canonical board)
        return str(board.pieces) + str(board.phase) + str(board.pieces_left) + str(board.moves_without_mills) + str(board.remove_piece)

    # to be changed
    @staticmethod
    def display(board):
        print(board.pieces)

