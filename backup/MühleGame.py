from __future__ import print_function
import sys
sys.path.append('..')
from Game import Game
from MühleLogic import Board
import numpy as np

"""
Game class implementation for the game of TicTacToe.
Based on the OthelloGame then getGameEnded() was adapted to new rules.

Author: Evgeny Tyurin, github.com/evg-tyurin
Date: Jan 5, 2018.

Based on the OthelloGame by Surag Nair.
"""
NUMBER_OF_FIELDS = 24

class TicTacToeGame(Game):

    actionList = []

    def __init__(self):

        self.actionList = self.getActionList()

    def getInitBoard(self):
        # return initial board
        b = Board()
        return b.pieces

    def getActionList(self):
        fields = range(NUMBER_OF_FIELDS)

        phase1_moves = [(-1, y) for y in fields]

        phase3_moves = [(x, y) for x in fields for y in fields if x!=y]

        remove_moves =[(x, -1) for x in fields]

        all_moves = phase1_moves + phase3_moves + remove_moves


        return all_moves


    def getBoardSize(self):
        # all possible fields
        return 24

    def getActionSize(self):
        # 600
        return len(self.actionList)

    def getNextState(self, board, phase, remove_piece,move_counter, player, action):
        # if player takes action on board, return next (board,player)
        # action must be a valid move
        move = self.actionList[action]

        b = Board()
        b.pieces = board.copy()
        b.phase = phase.copy()
        b.remove_piece = remove_piece
        b.moves_without_mills = move_counter
        b.execute_move(player, move)
        b.update_phase()

        if b.remove_piece:
            return b.pieces, b.phase, b.remove_piece, player
        else:
            return b.pieces, b.phase, b.remove_piece, -player



    def getValidMoves(self, board, player):

        valids = [0]*self.getActionSize()

        b = Board()
        b.pieces = board.pieces.copy()
        b.phase = board.phase.copy()
        b.remove_piece = board.remove_piece
        b.moves_without_mills = board.moves_without_mills


        legalMoves = board.get_legal_moves(player)
        for x, y in legalMoves:
            valid_move = self.actionList.index((x,y))
            valids[valid_move] = 1

        return np.array(valids)

        # return a fixed size binary vector


    def getGameEnded(self, board, phase, remove_piece, move_counter, player):
        # return 0 if not ended, 1 if player 1 won, -1 if player 1 lost
        # player = 1
        b = Board()
        b.pieces = board.copy()
        b.phase = phase.copy()
        b.remove_piece = remove_piece
        b.moves_without_mills = move_counter

        if b.is_win(player):
            return 1
        if b.is_win(-player):
            return -1
        if b.is_draw():
        # draw has a very little value
            return 1e-4

        if b.has_legal_moves():
            return 0



    def getCanonicalForm(self, board, player):
        # return state if player==1, else return -state if player==-1
        return player*board

    # to be changed
    def getSymmetries(self, board, pi):

        l =[(board,pi)]
        # mirror, rotational
       ''' assert(len(pi) == self.n**2+1)  # 1 for pass
        pi_board = np.reshape(pi[:-1], (self.n, self.n))
        l = []

        for i in range(1, 5):
            for j in [True, False]:
                newB = np.rot90(board, i)
                newPi = np.rot90(pi_board, i)
                if j:
                    newB = np.fliplr(newB)
                    newPi = np.fliplr(newPi)
                l += [(newB, list(newPi.ravel()) + [pi[-1]])]'''
        return l

    def stringRepresentation(self, board):
        # 8x8 numpy array (canonical board)
        return str(board)

    # to be changed
    @staticmethod
    def display(board):
        n = board.shape[0]

        print("   ", end="")
        for y in range(n):
            print (y,"", end="")
        print("")
        print("  ", end="")
        for _ in range(n):
            print ("-", end="-")
        print("--")
        for y in range(n):
            print(y, "|",end="")    # print the row #
            for x in range(n):
                piece = board[y][x]    # get the piece to print
                if piece == -1: print("X ",end="")
                elif piece == 1: print("O ",end="")
                else:
                    if x==n:
                        print("-",end="")
                    else:
                        print("- ",end="")
            print("|")

        print("  ", end="")
        for _ in range(n):
            print ("-", end="-")
        print("--")

