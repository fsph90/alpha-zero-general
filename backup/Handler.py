from MuehleLogic import Board
from MuehleGame import MuehleGame

from muehle.pytorch.NNet import NNetWrapper as NNet

from utils import *
from MCTS import MCTS
import numpy as np

class Handler():

    def __init__(self, pygame, DrawBoard, gameMode):
        self.dBoard = DrawBoard
        self.pygame = pygame
        self.gameMode = gameMode
        
        self.player = 1
        self.inPhase = False
        self.createdMill = False

        self.winner = 0

        self.board = Board()
        self.muehleGame = MuehleGame()

        if self.gameMode == 1:
            n1 = NNet(self.muehleGame)
            n1.load_checkpoint('','best.pth.tar')
            args1 = dotdict({'numMCTSSims': 50, 'cpuct': 1.0})
            mcts1 = MCTS(self.muehleGame, n1, args1)
            self.ai_player = lambda x: np.argmax(mcts1.getActionProb(x, temp=0))


        self.playing = True
        self.running = True

    def start(self):
        while self.running and self.playing:
            # mark legal moves

            self.markLegalMoves()

            for event in self.pygame.event.get():
                # Did the user click the window close button?
                if event.type == self.pygame.QUIT:
                    self.running = False
                if event.type == self.pygame.MOUSEBUTTONDOWN:
                    pos = self.pygame.mouse.get_pos()
                    self.checkPressedButton(pos)
                            
    def checkPressedButton(self,pos):
        (p1,p2) = pos

        for index in range(len(self.dBoard.place)):
            (x,y) = self.dBoard.place[index]
            if (x-9) <= p1 and (x+9) >= p1 and (y-9) <= p2 and (y+9) >= p2:
                self.handleMove(index)

    def handleMove(self, index):

        moves = self.board.get_legal_moves(self.player)
        valid = False
        phase = self.board.phase[self.player]

        if phase == 1: 
            for move in moves:
                (x,y) = move
                if y == index and x == -1:
                    valid = True
                    break
        elif phase == 2 or phase == 3:
            for move in moves:
                (x,y) = move
                if x == index:
                    valid = True
                    break
        

        if valid == False:
            return
        
        #check if move valid
        #check phase

        # 1 -> execute move when vaild
        if phase == 1:
            self.phase1Move(index)

        elif phase == 2:
            self.phase2Move(index)

        elif phase == 3:
            self.phase3Move(index)
                
        # 2 / 3 -> mark, show possible places, wait for new input
        # -> execute move when vaild afert second input 

        if self.gameMode == 1:
            self.AIstuff()
            print(self.board.phase)
            print(self.board.pieces_left)
        # check Ai mode
        # Do AI stuff
                
    def phase1Move(self, index):
        self.dBoard.changeColor(self.player, index)
        self.board.execute_move((-1,index), self.player)

        if self.board.remove_piece == True:
            self.doMill()

        # check if decided
        self.end()

        self.board.update_phase()
        self.player = -1 * self.player
        self.dBoard.setPlayerText(self.player, self.board.pieces_left[self.player])

    def phase2Move(self, index):
        self.dBoard.unMarkCircle()
        self.dBoard.setPlayerText(self.player, 0)
        self.dBoard.markCircle(index)
        # get legal moves
        moves = self.board.get_legal_moves(self.player)
        secLegalMoves = []

        for move in moves:
            (x,y) = move
            if x == index:
                secLegalMoves.append(y)
        # mark legal moves
        self.dBoard.markRangeCircles(secLegalMoves)
        # wait input
        secMove = self.startSecondMove(index, secLegalMoves)

        if secMove == index:
            self.dBoard.unMarkCircle()
            self.dBoard.setPlayerText(self.player, 0)
            return
        
        # ...

        # execute move
        self.board.execute_move((index, secMove), self.player)
        # reset Board
        self.dBoard.unMarkCircle()
        self.dBoard.changeColor(0, index)
        self.dBoard.changeColor(self.player, secMove)

        # do mill
        if self.board.remove_piece == True:
            self.doMill()
        
        # check if decided
        self.end()

        # change phase
        self.board.update_phase()
        # change player
        self.player = -1*self.player
        # changoe player text
        self.dBoard.setPlayerText(self.player, 0)
        # return

    def phase3Move(self, index):
        self.dBoard.unMarkCircle()
        self.dBoard.setPlayerText(self.player, 0)
        self.dBoard.markCircle(index)
        
        secLegalMoves = []

        # get legal moves
        moves = self.board.get_legal_moves(self.player)
        for move in moves:
            (x,y) = move
            if x == index:
                secLegalMoves.append(y)

        # mark legal moves
        self.dBoard.markRangeCircles(secLegalMoves)

        # wait input
        secMove = self.startSecondMove(index, secLegalMoves)

        if secMove == index:
            self.dBoard.unMarkCircle()
            self.dBoard.setPlayerText(self.player, 0)
            return
        
        # execute move
        self.board.execute_move((index, secMove), self.player)

        # reset Board
        self.dBoard.unMarkCircle()
        self.dBoard.changeColor(0, index)
        self.dBoard.changeColor(self.player, secMove)

        # do mill

        if self.board.remove_piece == True:
            self.doMill()
        
        # check if decided
        self.end()

        # change phase
        self.board.update_phase()
        # change player
        self.player = -1*self.player
        # changoe player text
        self.dBoard.setPlayerText(self.player, 0)
        # return
        
    def startSecondMove(self, index, legalMoves):
        while self.running:

            for event in self.pygame.event.get():
                # Did the user click the window close button?
                if event.type == self.pygame.QUIT:
                    self.running = False
                
                if event.type == self.pygame.MOUSEBUTTONDOWN:
                    pos = self.pygame.mouse.get_pos()
                    (p1,p2) = pos

                    # Check if unmark
                    if index is not -1:
                        (x,y) = self.dBoard.place[index]
                        if (x-9) <= p1 and (x+9) >= p1 and (y-9) <= p2 and (y+9) >= p2:
                            return index

                    # Check if second Field
                    for i in legalMoves:
                        (x,y) = self.dBoard.place[i]
                        if (x-9) <= p1 and (x+9) >= p1 and (y-9) <= p2 and (y+9) >= p2:
                            return i

    def doMill(self):
        moves = self.board.get_legal_moves(self.player)
        self.dBoard.unMarkCircle()
        self.dBoard.setPlayerText(self.player, self.board.pieces_left[self.player])

        legalMoves = []
        for move in moves:
            (x,y) = move
            legalMoves.append(x)

        self.dBoard.markRangeCircles(legalMoves)
        index = self.startSecondMove(-1, legalMoves)

        # execute move
        self.board.execute_move((index, -1), self.player)

        # change color
        self.dBoard.changeColor(0, index)
       
    def markLegalMoves(self):
        moves = self.board.get_legal_moves(self.player)
        legalMoves = []

        for move in moves:
            (x,y) = move
            if self.board.phase[self.player] == 1:
                legalMoves.append(y)
            else:
                legalMoves.append(x)
        # mark legal moves
        self.dBoard.markRangeCircles(legalMoves)

    def end(self):
        if self.board.is_win(self.player):
            self.winner = self.player
            self.playing = False
            

        if self.board.is_draw():
            self.winner = 0
            self.playing = False
              
    def AIstuff(self):
        
        # check phase
        phase = self.board.phase[self.player]

        if phase == 1:
            move = self.getAiAction()
            (x,y) = move
            # ecexute move
            self.board.execute_move(move, self.player, True)
            self.dBoard.changeColor(self.player, y)

        else:
            move = self.getAiAction()
            (x,y) = move
            # ecexute move
            self.board.execute_move(move, self.player)
            self.dBoard.changeColor(self.player, y)
            self.dBoard.changeColor(0,x)

        # check mill -> execute mill
        if self.board.remove_piece == True:
            move = self.getAiAction()
            (x,y) = move
            # execute move
            self.board.execute_move(move, self.player,True)
            self.dBoard.changeColor(0, x)
        
        self.board.update_phase()
        self.player = -1 * self.player
        self.dBoard.setPlayerText(self.player, self.board.pieces_left[self.player])

    def getAiAction(self):

        actions = self.muehleGame.getActionList()
        return actions[self.ai_player(self.muehleGame.getCanonicalForm(self.board, self.player))]